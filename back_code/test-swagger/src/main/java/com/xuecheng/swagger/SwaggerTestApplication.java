package com.xuecheng.swagger;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : YangZhanCheng
 * @ClassName :SwaggerTestApplication.java
 * @Description :
 * @createTime :2022年06月15日 20:25:00
 */
@EnableSwagger2Doc//开启swagger 加doc是启动类的swagger  不加是原生的swagger
@SpringBootApplication
public class SwaggerTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(SwaggerTestApplication.class,args);
    }
}
