package com.xuecheng.swagger.controller;

import com.xuecheng.swagger.pojo.Student;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @author : YangZhanCheng
 * @ClassName :HelloApi.java
 * @Description :
 * @createTime :2022年06月15日 21:42:00
 */
//tags: 文档上的组名
@Api(value = "hello Controller",tags = "hello Controller api 接口")
public interface HelloApi {

    @ApiOperation("测试方法")
    String hello();

    @ApiOperation("修改学员编号")
    @ApiImplicitParam(name = "num",value = "学员编号",required = true,defaultValue = "String",paramType = "query")
    Student modifyStudentNum( String num);


    @ApiOperation("修改学员姓名")
    @ApiImplicitParam(name = "name",value = "学员姓名",required = true,defaultValue = "String",paramType = "path")
    Student modifyStudentName( String name);


    @ApiOperation("修改学员")
    Student modifyStudent( Student student);


   @ApiOperation("修改学员编号")//修改文档中的方法名
   @ApiImplicitParams({
           //name: 形参名  value:形参的意思（文档中对参数的注解）   required: 是否必须传参   defaultValue: 参数类型    paramType：传参方式
           @ApiImplicitParam(name = "id",value = "学员的地址",required = true,defaultValue = "String",paramType = "path"),
           @ApiImplicitParam(name = "name",value = "学员的名称",required = true,defaultValue = "String",paramType = "query")
   })
    Student mofidyStudentBynNum( String id,  String name,  Student student);
}
