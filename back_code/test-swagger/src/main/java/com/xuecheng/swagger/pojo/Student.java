package com.xuecheng.swagger.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel(value = "stu 实体",description = "学员实体封装类")
public class Student {
    //学员编号
    @ApiModelProperty("学员编号")
    private String stuNo;
    //学成名称
    @ApiModelProperty("学员名称")
    private String name;
    //学员年龄
    @ApiModelProperty("学员年龄")
    private int age;
    //学员地址
    @ApiModelProperty("学员地址")
    private String address;
    
    //getter/setter 省略
}