package com.xuecheng.api.content;

import com.xuecheng.api.content.model.dto.CourseBaseDTO;
import com.xuecheng.api.content.model.qo.QueryCourseModel;
import com.xuecheng.api.content.model.vo.CourseBaseVO;
import com.xuecheng.common.domain.page.PageRequestParams;
import com.xuecheng.common.domain.page.PageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author : YangZhanCheng
 * @ClassName :CourseBaseApi.java
 * @Description :
 * @createTime :2022年06月16日 22:59:00
 */
@Api(value = "CourseBase api",tags = "课程基础信息api")
public interface CourseBaseApi {
    @ApiOperation("课程基础信息分页条件查询")
        //因为方法传参为实体类，在类中以增加文档注解，所以此处无需设置
    PageVO<CourseBaseDTO> queryCourseList(PageRequestParams params, QueryCourseModel model);
    @ApiOperation("课程基础信息添加")
    CourseBaseDTO createCourse(CourseBaseVO courseBaseVO);
    @ApiOperation("根据课程id查询课程基础信息数据")
    @ApiImplicitParam(name = "courseBaseId",value = "课程基础信息id值",required = true,dataType = "Long",paramType = "path")
    CourseBaseDTO getCourseById(Long courseBaseId);
    @ApiOperation("根据id修改课程基础信息")
    CourseBaseDTO modifyCourseById( CourseBaseVO courseBaseVO);
    @ApiOperation("根据id删除课程信息")
    @ApiImplicitParam(name = "courseBaseId",value = "课程id",required = true,dataType = "Long",paramType = "path")
    void removeCourseBaseById(Long courseBaseId);
}
