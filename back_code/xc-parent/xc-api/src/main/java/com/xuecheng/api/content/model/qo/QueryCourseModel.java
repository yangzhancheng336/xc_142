package com.xuecheng.api.content.model.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : YangZhanCheng
 * @ClassName :QueryCourseModel.java
 * @Description :
 * @createTime :2022年06月16日 23:01:00
 */
@Data
@ApiModel("课程基础信息查询封装对象")
public class QueryCourseModel {
    @ApiModelProperty("课程审核状态")
    private String auditStatus;
    @ApiModelProperty("课程名称")
    private String courseName;
}
