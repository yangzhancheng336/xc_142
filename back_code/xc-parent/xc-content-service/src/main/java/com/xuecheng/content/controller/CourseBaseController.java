package com.xuecheng.content.controller;

import com.xuecheng.api.content.CourseBaseApi;
import com.xuecheng.api.content.model.dto.CourseBaseDTO;
import com.xuecheng.api.content.model.qo.QueryCourseModel;
import com.xuecheng.api.content.model.vo.CourseBaseVO;
import com.xuecheng.common.domain.page.PageRequestParams;
import com.xuecheng.common.domain.page.PageVO;
import com.xuecheng.common.util.SecurityUtil;
import com.xuecheng.content.convert.CourseBaseConvert;
import com.xuecheng.content.entity.CourseBase;
import org.springframework.web.bind.annotation.*;
import com.xuecheng.content.service.CourseBaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 课程基本信息 前端控制器
 * </p>
 *
 * @author itcast
 */
@Slf4j
@RestController
public class CourseBaseController implements CourseBaseApi {

    @Autowired
    private CourseBaseService  courseBaseService;

    /*
     * springMAV默认请求下的接受参数类型为： QueryString(问号传参)
     *   QueryString: @RequestParam(在springMVC下默认可以省略，如果将其提成feign接口无法省略)
     *   Restful: @PathVariable
     *   RequestBody: @RequestBody
     */
    @Override
    @PostMapping("/course/list")
    public PageVO<CourseBaseDTO> queryCourseList(PageRequestParams params, @RequestBody QueryCourseModel model) {

//        1.获得请求头中信息并解析拿到公司id
        Long companyId = SecurityUtil.getCompanyId();

        PageVO<CourseBaseDTO> pageVO=courseBaseService.queryCourseList(params,model,companyId);
        return pageVO;
    }

    @Override
    @PostMapping("/course")
    public CourseBaseDTO createCourse(@RequestBody CourseBaseVO courseBaseVO) {
//        1.获得请求头中信息并解析拿到公司id
        Long companyId = SecurityUtil.getCompanyId();
//        2.将vo转换为dto
        CourseBaseDTO dto = CourseBaseConvert.INSTANCE.vo2Dto(courseBaseVO);
        dto.setCompanyId(companyId);
        return courseBaseService.createCourse(dto);
    }

    @Override
    @GetMapping("/course/{courseBaseId}")
    public CourseBaseDTO getCourseById(@PathVariable Long courseBaseId) {
        Long companyId = SecurityUtil.getCompanyId();
        return courseBaseService.getCourseById(courseBaseId,companyId);
    }

    @Override
    @PutMapping("/course")
    public CourseBaseDTO modifyCourseById(@RequestBody CourseBaseVO courseBaseVO) {
        Long companyId = SecurityUtil.getCompanyId();
        CourseBaseDTO dto = CourseBaseConvert.INSTANCE.vo2Dto(courseBaseVO);
        dto.setCompanyId(companyId);
        return courseBaseService.modifyCourseById(dto);
    }

    @Override
    @DeleteMapping("/course/{courseBaseId}")
    public void removeCourseBaseById(@PathVariable Long courseBaseId) {
        Long companyId = SecurityUtil.getCompanyId();
        courseBaseService.removeCourseBaseById(companyId,courseBaseId);
    }
}
