package com.xuecheng.content.convert;

import com.xuecheng.api.content.model.dto.CourseBaseDTO;
import com.xuecheng.api.content.model.vo.CourseBaseVO;
import com.xuecheng.content.entity.CourseBase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author : YangZhanCheng
 * @ClassName :CourseBaseConvert.java
 * @Description :
 * @createTime :2022年06月18日 01:39:00
 */
@Mapper
public interface CourseBaseConvert {
//    该接口的实例对象
    CourseBaseConvert INSTANCE= Mappers.getMapper(CourseBaseConvert.class);
//    用于标识不同的属性名之间的转换，只能放在单个转换方法上
    @Mappings(@Mapping( source = "id",target = "courseBaseId"))
    CourseBaseDTO entity2Dto(CourseBase courseBase);

    @Mappings(@Mapping( source = "courseBaseId",target = "id"))
    CourseBase dto2entity(CourseBaseDTO courseBaseDTO);



    CourseBaseDTO vo2Dto(CourseBaseVO courseBaseVO);
    /*
    * PS:集合数据的转换需要依赖单个数据的转换方法
    * */
    List<CourseBaseDTO> entitys2Dtos(List<CourseBase> courseBases);

}
