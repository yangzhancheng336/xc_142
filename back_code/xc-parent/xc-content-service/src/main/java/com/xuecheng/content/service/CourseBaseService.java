package com.xuecheng.content.service;

import com.xuecheng.api.content.model.dto.CourseBaseDTO;
import com.xuecheng.api.content.model.qo.QueryCourseModel;
import com.xuecheng.common.domain.page.PageRequestParams;
import com.xuecheng.common.domain.page.PageVO;
import com.xuecheng.content.entity.CourseBase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程基本信息 服务类
 * </p>
 *
 * @author itcast
 * @since 2022-06-16
 */
public interface CourseBaseService extends IService<CourseBase> {

    /**
     *  课程基础信息条件分页查询
     * @param params - PageRequestParams 分页封装对象
     * @param model - QueryCourseModel  课程条件封装对象
     * @param companyId -Long 教学机构ID
     * @return PageVO
     */
    PageVO<CourseBaseDTO> queryCourseList(PageRequestParams params, QueryCourseModel model,Long companyId);

    /**
     * 课程基础信息添加
     * @param dto - CourseBaseDTO 课程基础信息dto封装数据
     * @return  CourseBaseDTO 课程基础信息dto封装数据
     */
    CourseBaseDTO createCourse(CourseBaseDTO dto);

    /**
     *根据课程id查询基础信息
     * @param courseBaseId - Long 课程id
     * @param companyId - Long 公司id
     * @return
     */
    CourseBaseDTO getCourseById(Long courseBaseId, Long companyId);

    /**
     *根据id修改课程基础信息
     * @param dto - CourseBaseDTO 课程基础信息dto封装数据
     * @return
     */
    CourseBaseDTO modifyCourseById(CourseBaseDTO dto);

    /**
     * 根据id删除课程
     * @param companyId - Long 公司id
     * @param courseBaseId - Long 课程id
     */
    void removeCourseBaseById(Long companyId, Long courseBaseId);
}
