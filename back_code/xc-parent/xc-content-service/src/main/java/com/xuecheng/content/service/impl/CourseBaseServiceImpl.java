package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xuecheng.api.content.model.dto.CourseBaseDTO;
import com.xuecheng.api.content.model.qo.QueryCourseModel;
import com.xuecheng.common.domain.code.CommonErrorCode;
import com.xuecheng.common.domain.page.PageRequestParams;
import com.xuecheng.common.domain.page.PageVO;
import com.xuecheng.common.enums.common.CommonEnum;
import com.xuecheng.common.enums.content.CourseAuditEnum;
import com.xuecheng.common.enums.content.CourseChargeEnum;
import com.xuecheng.common.exception.ExceptionCast;
import com.xuecheng.common.util.StringUtil;
import com.xuecheng.content.common.constant.ContentErrorCode;
import com.xuecheng.content.convert.CourseBaseConvert;
import com.xuecheng.content.entity.CourseBase;
import com.xuecheng.content.entity.CourseMarket;
import com.xuecheng.content.mapper.CourseBaseMapper;
import com.xuecheng.content.service.CourseBaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xuecheng.content.service.CourseMarketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 课程基本信息 服务实现类
 * </p>
 *
 * @author itcast
 */
@Slf4j
@Service
public class CourseBaseServiceImpl extends ServiceImpl<CourseBaseMapper, CourseBase> implements CourseBaseService {

    @Autowired
    CourseMarketService courseMarketService;

    @Override
    /*
    * 功能实现：
    *   0. 是否开启事务 : 不需要
    *   1. 判断关键数据
    *   2. 构建分页对象Page
    *   3. 构建查询对象QueryWrapper
    *   4. 调用api查询结果
    *   5. 获得结果数据封装对象并返回
    * */
    public PageVO<CourseBaseDTO> queryCourseList(PageRequestParams params, QueryCourseModel model,Long companyId) {

//        1. 判断关键数据
        if (params.getPageNo()<1){
            params.setPageNo(PageRequestParams.DEFAULT_PAGE_NUM);
        }
        if (params.getPageSize()<1){
            params.setPageSize(PageRequestParams.DEFAULT_PAGE_SIZE);
        }
//        2. 构建分页对象Page
        Page<CourseBase> page = new Page<>(params.getPageNo(),params.getPageSize());

//        3. 构建查询对象QueryWrapper
        LambdaQueryWrapper<CourseBase> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CourseBase::getCompanyId,companyId);
        if (!(StringUtils.isEmpty(model.getAuditStatus()))){
            queryWrapper.eq(CourseBase::getAuditStatus,model.getAuditStatus());
        }
        if (!(StringUtils.isEmpty(model.getCourseName()))){
            queryWrapper.like(CourseBase::getName,model.getCourseName());
        }

//        4. 调用api查询结果
        Page<CourseBase> pageResult = this.page(page, queryWrapper);
        List<CourseBase> records = pageResult.getRecords();
        List<CourseBaseDTO> dtos = Collections.EMPTY_LIST;
        if (CollectionUtils.isEmpty(records)){
                dtos=new ArrayList<>(0);
        }else {

            dtos = CourseBaseConvert.INSTANCE.entitys2Dtos(records);
        }
        long total = pageResult.getTotal();


//        5. 获得结果数据封装对象并返回

        return new PageVO<CourseBaseDTO>(dtos, total, params.getPageNo(), params.getPageSize());
    }
    /*
    * 业务分析：
    *   0. 是否开启事务 ：增删改操作必加事务
    *   1. 判断关键数据 ：数据的关键性，如果没有，无法操作内容
    * 数据判断的依据： 1). 前端页面，只要对属性带红星的就代表必填数据
    *               2). 接口文档，必须为必填项
    *               3). 数据库表结构，有非空字段： company_id name mt st grade teachemode auditstatus
    *       判断课程的营销模式： 如果课程是收费课程，必须要改价格
    *   2.有些数据内容不是前端来维护，由后端来维护： id create-date change-date status  audit-status
    *   3. 添加数据到数据库 : 课程基础信息表： course-base    课程营销表 course-market(要关联course_id)
    *           对操作数据库的结果进行判断
    *   4. 返回操作后的结果数据dto: course-base   course-maker
    * */
    @Override
    @Transactional//开启事务
    public CourseBaseDTO createCourse(CourseBaseDTO dto) {
//        1. 判断关键数据 ：数据的关键性，如果没有，无法操作内容(只要不满足数据要求，要终止方法并返回错误数据)
//        数据判断的依据： 1). 前端页面，只要对属性带红星的就代表必填数据
//        2). 接口文档，必须为必填项
//        3). 数据库表结构，有非空字段： company_id name mt st grade teachemode
//        判断课程的营销模式： 如果课程是收费课程，必须要改价格
/*        if (ObjectUtils.isEmpty(dto.getCompanyId())||
                StringUtils.isEmpty(dto.getName())||
                StringUtils.isEmpty(dto.getMt())||
                StringUtils.isEmpty(dto.getSt())||
                StringUtils.isEmpty(dto.getTeachmode())||
                StringUtils.isEmpty(dto.getTags())){
            throw new RuntimeException("传入参数与接口不匹配");
        }*/
        String charge = verifyCourseMsg(dto);

//        2.有些数据内容不是前端来维护，由后端来维护： id(数据库自动生成)
//        create-date(mp) change-date(mp) status(数据库自动填充)
//        audit-status(后端业务自己维护)--对于新增课程的状态为：未提交
        dto.setAuditStatus(CourseAuditEnum.AUDIT_UNPAST_STATUS.getCode());

//        将dto数据转为po
        CourseBase courseBase = CourseBaseConvert.INSTANCE.dto2entity(dto);

//        3. 添加数据到数据库 : 课程基础信息表： course-base    课程营销表 course-market(要关联course_id)
//        课程基础信息表： course-base
//        对操作数据库的结果进行判断
//        确保id为null，只要id为null mp认定该数据为新数据
        courseBase.setId(null);

        boolean baseResult = this.save(courseBase);

        if (!baseResult) {
        throw new RuntimeException("课程基础信息保存失败");
        }
//        mp会对添加后的数据id进行回填
        Long id = courseBase.getId();

//        课程营销表 course-market(要关联course_id)
        CourseMarket courseMarket = new CourseMarket();
        courseMarket.setCourseId(id);
        courseMarket.setCharge(charge);
        BigDecimal price = dto.getPrice();
        courseMarket.setPrice(price.floatValue());

        boolean marketResult = courseMarketService.save(courseMarket);
        if (!marketResult){
            throw new RuntimeException("课程营销数据保存失败");
        }

//        4. 返回操作后的结果数据dto: course-base   course-maker
//    获取最新的数据内容: 需要查询
        CourseBase po = this.getById(id);
        CourseBaseDTO resultDto = CourseBaseConvert.INSTANCE.entity2Dto(po);
        resultDto.setCharge(dto.getCharge());
        resultDto.setPrice(dto.getPrice());

        return resultDto;

    }

    private String verifyCourseMsg(CourseBaseDTO dto) {
        if (ObjectUtils.isEmpty(dto.getCompanyId())) {
            // 业务异常：程序员在做业务判断时，数据有问题。
            // 异常：
            //    1.终止程序
            //    2.传递错误信息
            //    3.使用运行时异常来抛出
            //        运行时异常不需在编译期间处理
            throw new RuntimeException("公司id不能为空");
        }

        if (StringUtil.isBlank(dto.getName())) {
            throw new RuntimeException("课程名称不能为空");
        }

        if (StringUtil.isBlank(dto.getMt())) {
            throw new RuntimeException("课程大分类不能为空");
        }

        if (StringUtil.isBlank(dto.getSt())) {
            throw new RuntimeException("课程小分类不能为空");
        }

        if (StringUtil.isBlank(dto.getGrade())) {
            throw new RuntimeException("课程等级不能为空");
        }

        if (StringUtil.isBlank(dto.getTeachmode())) {
            throw new RuntimeException("课程教学模式不能为空");
        }

        if (StringUtil.isBlank(dto.getUsers())) {
            throw new RuntimeException("使用人群不能为空");
        }

        if (StringUtil.isBlank(dto.getCharge())) {
            throw new RuntimeException("课程收费不能为空");
        }

        // 判断收费课程，价格不能为空，必须要大于0
        // 课程为免费课程，价格为0
        String charge = dto.getCharge();
        String code = CourseChargeEnum.CHARGE_YES.getCode();
//        ObjectUtils.nullSafeEquals()  比较两个值是否一致
        if (ObjectUtils.nullSafeEquals(code,charge)){
            BigDecimal price = dto.getPrice();
            if (ObjectUtils.isEmpty(price)){
                throw new RuntimeException("收费价格不能为空");
            }
        }else {
//            免费课程价格默认赋值为0或null
            dto.setPrice(new BigDecimal("0.0"));
        }
        return charge;
    }

    /*
    *  1. 判断关键数据 ：前端传来的数据需要校验的内容
    *       courseBaseId  companyId
    *
    *  2. 判断业务数据： 根据关键数据来操作的数据内容，在后端存储
    *       判断课程信息是否存在
    *           根据id查询
    *       判断是否是同一加机构
    *           将数据库中的CourseBase里CompanyId和前端传过来的CompanId进行对比
    *       判断课程数据是否删除
    *           status： 1-使用 0-删除
    *  3. 将po数据转为dto并返回
    *       courseBase
    *       courseMarket
    * */
    @Override
    public CourseBaseDTO getCourseById(Long courseBaseId, Long companyId) {
        if (ObjectUtils.isEmpty(companyId)){
            throw new RuntimeException("公司id不能为空");
        }
        if (ObjectUtils.isEmpty(companyId)){
            throw new RuntimeException("课程id不能为空");
        }
        CourseBase courseBase = getCourseBaseByBaseId(courseBaseId,companyId);
        CourseBaseDTO dto = CourseBaseConvert.INSTANCE.entity2Dto(courseBase);
        CourseMarket courseMarket = getCourseMarketByCourseId(courseBaseId);
        dto.setCharge(courseMarket.getCharge());
        Float price = courseMarket.getPrice();
        dto.setPrice(new BigDecimal(price));


        return dto;
    }
    /*
    * 根据课程id查询课程营销数据
    * */
    private CourseMarket getCourseMarketByCourseId(Long courseBaseId) {
        LambdaQueryWrapper<CourseMarket> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CourseMarket::getCourseId,courseBaseId);
        CourseMarket courseMarket = courseMarketService.getOne(queryWrapper);
        if (ObjectUtils.isEmpty(courseMarket)){
            throw new RuntimeException("课程营销数据不存在");
        }
        return courseMarket;
    }

    /*
     * 根据课程id和公司id查询课程基础信息并判断课程是否被删除
     *
     * */
    private CourseBase getCourseBaseByBaseId(Long courseBaseId, Long companyId) {
        LambdaQueryWrapper<CourseBase> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CourseBase::getCompanyId,companyId);
        queryWrapper.eq(CourseBase::getId,courseBaseId);
        CourseBase courseBase = this.getOne(queryWrapper);
        if (ObjectUtils.isEmpty(courseBase)){
            throw new RuntimeException("课程不存在");
        }
        if (courseBase.getStatus().equals(CommonEnum.DELETE_FLAG.getCodeInt())){
            throw new RuntimeException("课程已被删除");
        }
        return courseBase;
    }
    /*
    * 业务分析
    *   0. 是否开启事务
    *   1. 判断关键数据
    *       可以调用添加时对关键数据的判断
    *       判断课程的id不能为空
    *   2. 判断业务数据
    *       课程基础信息
    *           判断是否存在
    *           判断是否是同一家公司
    *           判断是否删除
    *           判断课程审核状态
    *               未提交和审核未通过才可以进行修改
    *           课程营销数据
    *   3. 修改数据
    *       修改课程基础数据
    *       修改课程营销数据
    *           如果是收费课程，直接覆盖原数据（charge，price）
    *           如果是免费课程，直接覆盖原数据（charge，price=0.0）
    *   4.将数据库最新数据返回给前端
    *       courseBase  courseMarket
    * */
    @Override
    @Transactional
    public CourseBaseDTO modifyCourseById(CourseBaseDTO dto) {
        verifyCourseMsg(dto);
        Long courseBaseId = dto.getCourseBaseId();
        if (ObjectUtils.isEmpty(courseBaseId)){
            throw new RuntimeException("修改课程id不能为空");
        }
        getCourseBaseByLogic(courseBaseId,dto.getCompanyId());
        CourseMarket courseMarketByCourseId = getCourseMarketByCourseId(courseBaseId);
        CourseBase courseBase = CourseBaseConvert.INSTANCE.dto2entity(dto);
        courseBase.setCompanyId(null);
        courseBase.setAuditStatus(null);
        courseBase.setStatus(null);
        boolean baseResult = this.updateById(courseBase);
        if (!baseResult){
            throw new RuntimeException("课程信息修改失败");
        }
        LambdaUpdateWrapper<CourseMarket> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(CourseMarket::getCharge,dto.getCharge());
        String charge = dto.getCharge();
        if (CourseChargeEnum.CHARGE_YES.getCode().equals(charge)){
            if (ObjectUtils.isEmpty(dto.getPrice())){
                throw new RuntimeException("收费课程价格不能为空");
            }
            updateWrapper.set(CourseMarket::getPrice,dto.getPrice());
        }else {
            updateWrapper.set(CourseMarket::getPrice,0F);
        }
        updateWrapper.set(CourseMarket::getCharge,charge);
        boolean update = courseMarketService.update(updateWrapper);
        if (!update){
            throw new RuntimeException("修改营销数据失败");
        }
        CourseBaseDTO resultDTO = getLastCourseBaseDTO(dto,courseBaseId);
        return resultDTO;
    }

    @Override
    public void removeCourseBaseById(Long companyId, Long courseBaseId) {
        if (ObjectUtils.isEmpty(companyId)||ObjectUtils.isEmpty(courseBaseId)){
            ExceptionCast.cast(CommonErrorCode.E_100101);
        }
        getCourseBaseByLogic(courseBaseId,companyId);
        LambdaUpdateWrapper<CourseBase> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(CourseBase::getStatus,CommonEnum.DELETE_FLAG.getCodeInt());
        updateWrapper.set(CourseBase::getChangeDate, LocalDateTime.now());
        updateWrapper.eq(CourseBase::getId,courseBaseId);
        boolean update = update(updateWrapper);
        if (!update){
            ExceptionCast.cast(ContentErrorCode.E_120017);
        }

    }

    /*
    * 根据课程id查询最新数据并封装
    * */
    private CourseBaseDTO getLastCourseBaseDTO(CourseBaseDTO dto, Long courseBaseId) {
        CourseBase po = this.getById(courseBaseId);
        CourseBaseDTO resultDto = CourseBaseConvert.INSTANCE.entity2Dto(po);
        resultDto.setCharge(dto.getCharge());
        resultDto.setPrice(dto.getPrice());
        return resultDto;
    }
    /*
    * 根据公司id和课程id查询课程基础信息并判断课程审核状态是否符合修改要求
    * */
    private void getCourseBaseByLogic(Long courseBaseId, Long companyId) {
        CourseBase courseBase = getCourseBaseByBaseId(courseBaseId, companyId);
        String auditStatus = courseBase.getAuditStatus();
        if (CourseAuditEnum.AUDIT_COMMIT_STATUS.getCode().equals(auditStatus)
                ||CourseAuditEnum.AUDIT_PUBLISHED_STATUS.getCode().equals(auditStatus)
                ||CourseAuditEnum.AUDIT_PASTED_STATUS.getCode().equals(auditStatus)){
            throw new RuntimeException("课程审核状态异常");
        }
    }
}
