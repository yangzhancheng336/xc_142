package com.xuecheng.content.test;


import com.xuecheng.api.content.model.dto.CourseBaseDTO;
import com.xuecheng.content.convert.CourseBaseConvert;
import com.xuecheng.content.entity.CourseBase;
import org.junit.Test;

/**
 * @author : YangZhanCheng
 * @ClassName :MapStructTest.java
 * @Description :
 * @createTime :2022年06月18日 01:36:00
 */
public class MapStructTest {
    @Test
    public void testMap(){
//        1.构建po对象
        CourseBase courseBase = new CourseBase();
        courseBase.setId(111L);
        courseBase.setName("spring高级课程");
//        2.使用MapStruct来做属性映射
        CourseBaseDTO dto = CourseBaseConvert.INSTANCE.entity2Dto(courseBase);
        System.out.println(dto);
    }
}
