package com.xuecheng.content.service;

import com.xuecheng.content.entity.CourseBaseDTO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程基本信息 服务类
 * </p>
 *
 * @author itcast
 * @since 2022-06-18
 */
public interface CourseBaseService extends IService<CourseBaseDTO> {

}
