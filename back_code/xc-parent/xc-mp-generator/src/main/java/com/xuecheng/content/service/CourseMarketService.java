package com.xuecheng.content.service;

import com.xuecheng.content.entity.CourseMarketDTO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程营销信息 服务类
 * </p>
 *
 * @author itcast
 * @since 2022-06-18
 */
public interface CourseMarketService extends IService<CourseMarketDTO> {

}
